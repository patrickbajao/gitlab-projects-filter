require 'cgi'
require 'yaml'
require 'uri'
require 'net/http'
require 'json'

PROJECTS_FILE = 'projects.yml'
OUTPUT_FILE = 'output.txt'

token = ENV['GITLAB_TOKEN']
language = ARGV[0]

unless token
  puts 'Please set GITLAB_TOKEN env var when running. Example: `GITLAB_TOKEN=abc123 ruby filter.rb Go`.'
  exit 1
end

unless language
  puts 'Please set a language to filter by. Example: `ruby filter.rb Go`.'
  exit 1
end

unless File.exist?(PROJECTS_FILE)
  puts 'Please ensure that projects.yml file exists in the project root dir.'
end

projects = YAML.load_file(PROJECTS_FILE)

puts "Filtering projects with #{language} language..."

filtered_projects = []

projects.each do |key, project|
  print "Checking if #{project['name']} uses #{language} language..."

  url_encoded_path = CGI.escape(project['path'])
  uri = URI("https://gitlab.com/api/v4/projects/#{url_encoded_path}/languages")
  
  res = Net::HTTP.start(uri.hostname, uri.port, use_ssl: true) do |http|
    req = Net::HTTP::Get.new(uri)
    req['PRIVATE-TOKEN'] = token
    http.request(req)
  end

  if res.is_a?(Net::HTTPSuccess)
    languages = JSON.parse(res.body)

    if languages.include?(language)
      filtered_projects << { name: project['name'], link: project['link'] }
      puts 'yes!'
    else
      puts 'nope.'
    end
  else
    puts 'failed.'
  end

  # Sleep for 5 seconds so we don't spam API endpoint
  sleep 5
end

puts 'Done!'

print "Generating #{OUTPUT_FILE}..."

File.open(OUTPUT_FILE, 'w+') do |f|
  filtered_projects.each do |fp|
    f.write("#{fp[:name]} - #{fp[:link]}\n")
  end
end

puts 'done!'
